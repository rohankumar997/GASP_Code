try:
    from gasp import *
except:
    _have_gasp = 0
else:
    _have_gasp = 1


try:
    from gasp import games
except:
    _have_games = 0
else:
    _have_games = 1


try:
    import pygame
except:
    _have_pygame = 0
else:
    _have_pygame = 1

if _have_gasp:
    print("GASP is installed")
if _have_pygame and _have_games:
    print("Games is installed")
