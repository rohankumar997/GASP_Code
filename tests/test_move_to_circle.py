import time

from gasp import *

messages = ["Here is a circle", "Moving it right", "Moving it to the left", "Stay still...","Don't Move!!","Done!"]
begin_graphics(600, 600)
text = Text(messages[0], (300, 500))
circle = Circle((300, 300), 50, filled=True)

time.sleep(2)
remove_from_screen(text)
text = Text(messages[1], (300, 500))
move_to(circle, (400, 300))

time.sleep(2)
remove_from_screen(text)
text = Text(messages[2], (300, 500))
move_to(circle, (200, 300))

time.sleep(2)
remove_from_screen(text)
text = Text(messages[3], (300, 500))
move_to(circle, (200, 300))

time.sleep(2)
remove_from_screen(text)
text = Text(messages[4], (300, 500))
move_to(circle, (200, 300))

time.sleep(2)
remove_from_screen(text)
text = Text(messages[5], (300, 500))


end_graphics()
