# Note: Great reference for Python tkinter:
# https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/index.html
import time

from gasp import *

begin_graphics(800, 600)

# Draw a Circle and Box with the same coordinates and size
circle = Circle((400, 300), 200)
square = Box((200, 100), 400, 400)
time.sleep(3)

end_graphics()
