# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.6.2] - 2022-9-7
### Fixed
- gasp Text size parameter now changes the size properly

## [0.6.1] - 2021-7-28
### Added
- games Ellipse class
- games Arc class
- games Circle now has a thickness parameter
- games Animation play_animation
- games Animation image_changed
### Changed
- games Circle class is now a subclass of Ellipse
- games collision detection is now pixel perfect
- games load_animation now only loads one animation list at a time
### Fixed
- games replace_image no longer leaves ghost object
- games Animation init_animation angle parameter is now used

## [0.6.0] - 2021-7-11
### Added
- games Screen set_title()
- games Line repr
- boards GameCell grid_coords() and screen_coords()
- GASP cards module (standard deck of cards API)
    - classes: Card, Deck, SingleDeck, Pile
    - see sheet [K-cards](http://www.openbookproject.net/pybiblio/gasp/course/K-cards.html) for more details
- games Object collidepoint()
- boards SingleBoard init_singleboard now takes an optional title parameter
- utils add_vectors()
- utils distance()
- games Text Font Functionality
    - init_text() now has an optional font parameter
    - set_font()
    - available_fonts()
### Fixed
- color from_str() no longer calls eval(); now safe to use
- games _fix_offsets() is no longer overridden by subclasses of Object

## [0.5.2] - 2021-6-13
### Added
- games Screen center_pos()
- games Screen get_width()
- games Screen get_height()
- games Line class
- games flip_image utility function
- games set_resolution utility function
### Changed
- boards Container class is no longer designed as a mix-in. See sheet
[B-boards](http://www.openbookproject.net/pybiblio/gasp/course/B-boards.html) for more details
- boards GameCell objects automatically raise any non-GridCell type objects that overlap them
### Fixed
- games move_to() no longer creates duplicate items
- games Animation no longer crashes on startup

## [0.5.1] - 2021-6-2
Complete overhaul of GASP from Python 2 to Python 3.  
This is the minimum required version to run most GASP programs.  
See [AUTHORS.md](https://codeberg.org/GASP/GASP_Code/src/branch/main/AUTHORS.md) for credits.

## [0.5.0] - 2021-6-1
Unusable version of GASP3 uploaded to PyPI. Installation includes dist-info, but no actual code.  
See v0.5.1 above.

## [0.4.x and below] - circa 2007 and below
These old versions of GASP are built on Python 2, which has been deprecated as of 2020.  
By extension, all versions of GASP in the form 0.4.x or below have also been deprecated.


#### Version Links
[[0.6.1]](https://codeberg.org/GASP/GASP_Code)  
[[0.6.0]](https://codeberg.org/GASP/GASP_Code/src/commit/f95493265458190ee805c55d1291ead478b6a2a2)  
[[0.5.2]](https://codeberg.org/GASP/GASP_Code/src/commit/45d4193f65fd7a68093b6321d401efbc39097222)  
[[0.5.1]](https://codeberg.org/GASP/GASP_Code/src/commit/736b720eafb8b7ebc93a06a44d2dcf44548a602c)  